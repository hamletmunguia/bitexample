**Ejemplo del uso de bibucket para Academia Picap**

Como se fue solicitando en el ejercicio pr ctico, se fue agregando una fila, en el repo se ven dos ramas creadas:
1. dev_contenido: Que es donde se agrega el html donde se va estructurando la tabla
2. dev_estilos: Que es donde se agregan los estilos de la tabla.

Anexo evidencia de lo solicitado:

**Primer commit**

![alt text](images/first_commit.png)

**Segundo commit**

![alt text](images/last_commit.png)
